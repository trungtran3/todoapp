import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'package:todo_app/contact/contact.dart';


class NewContactForm extends StatefulWidget {
  @override
  _NewContactFormState createState() => _NewContactFormState();
}

class _NewContactFormState extends State<NewContactForm> {
  final _formKey = GlobalKey<FormState>();

  String _name;
  
  void addContact(Contact contact) {
    final contactBox = Hive.box('contacts');
    contactBox.add(contact);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    
                    hintText: 'Write Here !!!',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    ),
                  onSaved: (value) => _name = value,
                ),
              ),
              
               TextButton(
                child: Text('AddTodo',
                  style: TextStyle(
                    color: Colors.white
                  ),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: () {
                  
                  _formKey.currentState.save();
                  final newContact = Contact(_name,);
                  addContact(newContact);
                },
              )
            ],
            
          ),
          
        ],
      ),
    );
  }
}
