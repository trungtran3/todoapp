import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class CheckBox extends StatefulWidget {
  const CheckBox({Key key}) : super(key: key);

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool selected = false;


  @override
  Widget build(BuildContext context) {
    return Checkbox(
       
        value: selected,
        activeColor: Colors.white,
        checkColor: Colors.black,
        onChanged: (value) {
          setState(() {
            this.selected = !this.selected;
            
          });
        });

        
  }
}
