import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:todo_app/contact/contact.dart';

import 'add_todo.dart';
import 'check_box.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Center(
            child: Text(
              'IOD TODO APP',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.fromLTRB(50, 50, 50, 20),
          child: Column(
            children: <Widget>[
              NewContactForm(),
              Expanded(child: _buildListView()),
            ],
          ),
        ));
  }

  Widget _buildListView() {
    return WatchBoxBuilder(
      box: Hive.box('contacts'),
      builder: (context, contactsBox) {
        return ListView.builder(
          itemCount: contactsBox.length,
          itemBuilder: (BuildContext context, int index) {
            final contact = contactsBox.getAt(index) as Contact;
            return Container(
              decoration: BoxDecoration(
                border: Border(bottom: (BorderSide(width: 1, color: Colors.black12)) ),
              ),
              child: ListTile(
                
                title: Text(
                  
                  contact.name,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    /* decoration: (CheckBox.selected) ? TextDecoration.lineThrough : null, */
                    
                  ),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CheckBox(),
                    Container(
                      decoration: const BoxDecoration(
                        color: Colors.red,
                      ),
                      child: IconButton(
                        icon: Icon(Icons.delete, color: Colors.white),
                        onPressed: () {
                          contactsBox.deleteAt(index);
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
            
          },
        );
      },
    );
  }
}
